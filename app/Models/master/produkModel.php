<?php

namespace App\Models\master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
class produkModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_produk';
    protected $primaryKey  = 'id_produk';   
    protected $guarded = [];
    public static function get_data(){
        return DB::table('m_produk')
        ->select(
            'm_produk.id_ukuran', 
            'm_jenis_produk.id_jenis', 
            'm_jenis_produk.nama_jenis', 
            'm_produk.id_produk', 
            'm_jenis_ukuran.kode_ukuran', 
            'm_produk.kode_produksi',
            'm_produk.gender',  
            'm_produk.nama_produk', 
            'm_produk.stock', 
            'm_produk.thumbnail', 
            'm_produk.harga', 
            'm_produk.link', 
            'm_produk.created_at'
        )
        ->leftJoin('m_jenis_ukuran', 'm_jenis_ukuran.id_ukuran', '=', 'm_produk.id_ukuran')
        ->leftJoin('m_jenis_produk', 'm_jenis_produk.id_jenis', '=', 'm_produk.id_jenis')
        ->where('m_produk.deleted_at','=',NULL )
        ->orderBy('m_produk.created_at','ASC')
        ->get();
    }

    public static function get_data_by_id($id){
        return DB::table('m_produk')
        ->select(
            'm_produk.id_ukuran', 
            'm_jenis_produk.id_jenis', 
            'm_jenis_produk.nama_jenis', 
            'm_produk.id_produk', 
            'm_jenis_ukuran.kode_ukuran', 
            'm_produk.kode_produksi', 
            'm_produk.nama_produk', 
            'm_produk.stock', 
            'm_produk.gender',  
            'm_produk.thumbnail', 
            'm_produk.harga', 
            'm_produk.link', 
            'm_produk.created_at'
        )
        ->leftJoin('m_jenis_ukuran', 'm_jenis_ukuran.id_ukuran', '=', 'm_produk.id_ukuran')
        ->leftJoin('m_jenis_produk', 'm_jenis_produk.id_jenis', '=', 'm_produk.id_jenis')
        ->where('m_produk.deleted_at','=',NULL )
        ->where('m_produk.id_produk','=',$id )
        ->orderBy('m_produk.created_at','ASC')
        ->first();
    }

    public static function get_count_data_include_delete(){
        return DB::table('m_produk')
        ->select(
            'm_produk.id_ukuran', 
            'm_jenis_produk.id_jenis', 
            'm_jenis_produk.nama_jenis', 
            'm_produk.id_produk', 
            'm_jenis_ukuran.kode_ukuran', 
            'm_produk.kode_produksi', 
            'm_produk.nama_produk', 
            'm_produk.stock', 
            'm_produk.thumbnail', 
            'm_produk.harga', 
            'm_produk.link', 
            'm_produk.created_at'
        )
        ->leftJoin('m_jenis_ukuran', 'm_jenis_ukuran.id_ukuran', '=', 'm_produk.id_ukuran')
        ->leftJoin('m_jenis_produk', 'm_jenis_produk.id_jenis', '=', 'm_produk.id_jenis')
        ->orderBy('m_produk.created_at','ASC')
        ->count();
    }

}
