@extends('layout.app')
@section('pagespecificcss')
@endsection
@php
use SimpleSoftwareIO\QrCode\Facades\QrCode;
@endphp
@if (app()->isLocal())
<!-- Datatable -->
<link href="{{ asset('template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<!-- Custom Stylesheet -->
<link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
@else
<!-- Datatable -->
<link href="{{ asset('public/template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<!-- Custom Stylesheet -->
<link href="{{ asset('public/template/css/style.css') }}" rel="stylesheet">
@endif
@section('content')

    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Hi, Admin</h4>
                        <p class="mb-0">Generate Barcode</p>
                    </div>
                </div>
                <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Generate</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Barcode</a></li>
                    </ol>
                </div>
            </div>
            <div class="row">

                @if ($errors->any())
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </strong>
                        </div>
                    </div>

                @endif
                @if (Session::has('msg'))
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                Data Berhasil Dihapus
                            </strong>
                        </div>
                    </div>

                @endif
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Generate Barcode</h4>
                            <button type="button" class="badge badge-rounded badge-primary" data-toggle="modal"
                                data-target=".bd-example-modal-lg">Tambah Barcode</button>
                        </div>
                        <div class="card-body">
                            Toggle column: <a class="toggle-vis" data-column="1">Kode Produksi</a> - <a
                                class="toggle-vis" data-column="2">Link QR Code</a> - <a class="toggle-vis"
                                data-column="3">thumbnail</a> - <a class="toggle-vis" data-column="4">Nama Produk</a> -
                            <a class="toggle-vis" data-column="5">Size</a> - <a class="toggle-vis"
                                data-column="6">Jumlah Stock</a> - <a class="toggle-vis" data-column="7">Harga</a>
                            <div class="table-responsive">
                                <table id="datatable" class="display" style="min-width: 200px;display:block">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Produksi</th>
                                            <th>Link QR Code</th>
                                            <th style="max-width: 10%">thumbnail</th>
                                            <th>Nama Produk</th>
                                            <th>Size</th>
                                            <th>gender</th>
                                            <th>Jumlah Stock</th>
                                            <th>Harga</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                       


                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Produksi</th>
                                            <th>Link QR Code</th>
                                            <th style="max-width: 10%">thumbnail</th>
                                            <th>Nama Produk</th>
                                            <th>Size</th>
                                            <th></th>
                                            <th>Jumlah Stock</th>
                                            <th>Harga</th>
                                            <th>#</th>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                        <div class="card-footer">
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificjs')

@endsection
@if (app()->isLocal())
<script src="{{ asset('template/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
@else
<script src="{{ asset('public/template/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('public/template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>

@endif
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "scrollY": "400px",

        });

        $('a.toggle-vis').on('click', function(e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });


    });
</script>
