<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTKodeProduksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kode_produksi', function (Blueprint $table) {
                $table->uuid('id_kode_produksi')->primary();
                $table->string('kode_produksi');
                $table->string('detail_kode_produksi');
                $table->string('keterangan');
                $table->timestamps();
                $table->softDeletesTz($column = 'deleted_at', $precision = 0);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kode_produksi');
    }
}
