<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title> Encore </title>
    <!-- Favicon icon -->

    @if (app()->isLocal())
        <link rel="stylesheet" href="{{ asset('/template/vendor/owl-carousel/css/owl.carousel.min.css') }}">
        <link rel="stylesheet"
            href="{{ asset('/template/vendor/owl-carousel/css/owl.theme.default.min.css') }}">
        <link href="{{ asset('/template/vendor/jqvmap/css/jqvmap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/template/css/style.css') }}" rel="stylesheet">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/logo.png') }}">
    @else
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/img/logo.png') }}">

        <link rel="stylesheet" href="{{ asset('public/template/vendor/owl-carousel/css/owl.carousel.min.css') }}">
        <link rel="stylesheet"
            href="{{ asset('public/template/vendor/owl-carousel/css/owl.theme.default.min.css') }}">
        <link href="{{ asset('public/template/vendor/jqvmap/css/jqvmap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/template/css/style.css') }}" rel="stylesheet">
    @endif

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
     Nav header start
    ***********************************-->

    @include('layout.header')

    <!--**********************************
     Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
    sidebar start
    ***********************************-->
        @include('layout.sidebar')
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        @yield('content')
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Quixkit</a> 2019</p>
                <p>Distributed by <a href="https://themewagon.com/" target="_blank">Themewagon</a></p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    @include('layout.dashboardjs')



</body>

</html>
