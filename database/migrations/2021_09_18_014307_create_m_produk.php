<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_produk', function (Blueprint $table) {
            $table->uuid('id_produk')->primary();
            $table->uuid('id_ukuran');
            $table->uuid('id_jenis');
            $table->string('kode_produksi');
            $table->string('nama_produk');
            $table->string('gender');
            $table->integer('stock');
            $table->bigInteger('harga');
            $table->string('link');
            $table->string('thumbnail');
            $table->timestamps();
            $table->softDeletesTz($column = 'deleted_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_produk');
    }
}
