<?php

namespace App\Http\Controllers\transaksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;
// Request Models
use App\Models\master\kategoriProdukModel;
use App\Models\master\ukuranModel;
use App\Models\master\produkModel;
use App\Models\transaksi\t_kode_produksi;
use PDF;
use Image;

class kode_produksiController extends Controller
{
    public function index($id)
    {
        $data['data_produk'] = t_kode_produksi::get_data(decrypt($id));
        $data['id_produk'] = decrypt($id);
        $data['produk_nm']=produkModel::where('kode_produksi', decrypt($id))->first();
        $check_data = t_kode_produksi::where('kode_produksi', '=', decrypt($id))->first();
        if ($check_data == NULL) {
            $produk = produkModel::where('kode_produksi', decrypt($id))->first();
            $angka = 0;

            $uuid = Str::uuid();
            for ($i = 0; $i < $produk->stock; $i++) {
                $kode = decrypt($id);
                do {

                    $uuid = Str::uuid();
                } while (produkModel::find($uuid));
                t_kode_produksi::create([
                    'id_kode_produksi' => $uuid,
                    'kode_produksi' => $kode,
                    'detail_kode_produksi' => $kode . (sprintf("%'.05d", t_kode_produksi::get_count_data_include_delete(decrypt($id)) + 1)),
                    'keterangan' => "-"
                ]);
                $angka++;
            }
        }
 
        return view('transaksi.kode_produksi', $data);
    }

    public function cetak_all($id){
        $get = t_kode_produksi::get_data($id);
        
        foreach($get as $row){
            $change = t_kode_produksi::find($row->id_kode_produksi);
            $change->status = 1;
            $change->save();
        }
 
        $data['t_kode_produksi'] = t_kode_produksi::get_data($id);
   
        $pdf = PDF::loadView    ('pdf/barcode_generator_all', $data);
        $customPaper = array(0, 0, 1000, 500);
        $pdf->setPaper($customPaper);
        return $pdf->stream('print.pdf');

    }
    
    public function generate_barcode($id)
    {
        $get= t_kode_produksi::get_first(decrypt($id));
        $data = t_kode_produksi::find($get->id_kode_produksi);
		$data->status = 1;
        $data->save();
        $data['data']=$get;
      
        $data['t_kode_produksi'] = t_kode_produksi::get_first(decrypt($id));
  
        $pdf = PDF::loadView('pdf/barcode_generator', $data);
        $customPaper = array(0, 0, 1000, 500);
        $pdf->setPaper($customPaper);
        return $pdf->stream('cetak_produk.pdf');
    }
}
