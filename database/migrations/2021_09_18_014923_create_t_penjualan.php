<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_penjualan', function (Blueprint $table) {
             
                $table->uuid('id_transaksi')->primary();
                $table->uuid('id_produk');
                $table->uuid('id_pembeli');
                $table->string('nama_pembeli');
                $table->string('keterangan');
                $table->string('alamat_pembeli');
                $table->string('kode_pos');
                $table->string('no_hp');
                $table->integer('jumlahbeli');
                $table->timestamps();
                $table->softDeletesTz($column = 'deleted_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_penjualan');
    }
}
