<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-text"><a href="{{ url('/apps') }}">Dashboard</a></li>
            <li class="nav-label first">Main Menu</li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                        class="icon icon-single-04"></i><span class="nav-text">Management</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ url('/produk') }}">Produk</a></li>
                    <li><a href="#">Transaksi</a></li>
                </ul>
            </li>
            <li class="nav-label">Pengaturan</li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                        class="icon icon-app-store"></i><span class="nav-text">Setting</span></a>
                <ul aria-expanded="false">
                    <li><a href="#">Menu 1</a></li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Drop down Menu 1</a>
                        <ul aria-expanded="false">
                            <li><a href="#">Menu 2</a></li>
                            <li><a href="#">Menu 3</a></li>
                            <li><a href="#">Menu 4</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/blank') }}">Blank page</a></li>
                </ul>
            </li>
             
        </ul>
    </div>


</div>