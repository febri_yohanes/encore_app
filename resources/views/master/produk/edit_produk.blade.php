@extends('layout.app')
@section('pagespecificcss')
@endsection
@php
use SimpleSoftwareIO\QrCode\Facades\QrCode;
@endphp
@if (app()->isLocal())
    <!-- Datatable -->
    <link href="{{ asset('template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <!-- Custom Stylesheet -->
    <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
@else
    <!-- Datatable -->
    <link href="{{ asset('public/template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <!-- Custom Stylesheet -->
    <link href="{{ asset('public/template/css/style.css') }}" rel="stylesheet">
@endif
@section('content')

    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Form Edit Produk</h4>
                        <p class="mb-0">Management Produk</p>
                    </div>
                </div>
                <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Produk</a></li>
                    </ol>
                </div>
            </div>
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Rubah Data Produk</h4>

                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ url('/produk/proses_update_data') }}" id="form1"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <div class="form-group">
                                    <label>Masukan Nama produk</label>
                                    <input type="hidden" name="id_produk" value="{{ $data_produk->id_produk }}">
                                    <input type="text" name="nama_produksi" value="{{ $data_produk->nama_produk }}"
                                        class="form-control input-default " placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="number" name="harga" value="{{ $data_produk->harga }}"
                                        class="form-control input-default " placeholder="Rp.">
                                </div>
                                <div class="form-group">
                                    <label>Jumlah Stock Barang</label>
                                    <input type="number" name="stock" class="form-control input-default "
                                        value="{{ $data_produk->stock }}" placeholder="Jumlah Stock Barang">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <input type="text" class="form-control input-default " value="<?php if ($data_produk->gender == 'm') {
                                                echo 'Male';
                                            } else {
                                                echo 'Female';
                                            } ?>" disabled>
                                </div>

                                <div class="form-group">
                                    <label>Ukuran / Size Baju</label>
                                    <select class="form-control" name="size" id="sel1">
                                        @foreach ($ukuran_baju as $item)
                                            <option value="{{ $item->id_ukuran }}" @php
                                                if ($data_produk->id_ukuran == $item->id_ukuran) {
                                                    echo 'Selected';
                                                }
                                            @endphp>
                                                {{ $item->kode_ukuran }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Produk</label>
                                    <select class="form-control" name="nama_jenis" id="sel1">
                                        @foreach ($kategori as $item)
                                            <option value="{{ $item->id_jenis }}" @php
                                                if ($data_produk->id_jenis == $item->id_jenis) {
                                                    echo 'Selected';
                                                }
                                            @endphp>
                                                {{ $item->nama_jenis }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Link QR Code</label>
                                    <input type="text" name="qrcode" value="{{ $data_produk->link }}"
                                        class="form-control input-default " placeholder="https:://">
                                </div>
                                <div class="form-group">
                                    <label>Thumbnail</label>
                                    <img src="{{ asset('upload/thumbnail/') . '/' . $data_produk->thumbnail }}"
                                        style="display:block;" width="10%" height="15%" alt="image">
                                    <input type="file" name="thumbnail">


                                </div>
                                <div class="form-group">
                                    <a href="{{ url('/produk') }}" class="btn btn-secondary">Kembali</a>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <br>
                                    <hr>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">

                        </div>



                    </div>

                </div>
                @if ($errors->any())
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </strong>
                        </div>
                    </div>

                @endif
                @if (Session::has('msg'))
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                Data Berhasil Dirubah
                            </strong>
                        </div>
                    </div>

                @endif
            </div>
        </div>
    </div>
    </div>

@endsection

@section('pagespecificjs')

@endsection
@if (app()->isLocal())
    <script src="{{ asset('template/vendor/global/global.min.js') }}"></script>
    <script src="{{ asset('template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
@else
    <script src="{{ asset('public/template/vendor/global/global.min.js') }}"></script>
    <script src="{{ asset('public/template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
@endif
<script>

</script>
