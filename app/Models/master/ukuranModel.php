<?php

namespace App\Models\master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
class ukuranModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_jenis_ukuran';
    protected $primaryKey  = 'id_ukuran';
    protected $fillable = ['id_ukuran'];
}
