<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\master\produkModel;

class AppController extends Controller
{
    public function index(){
        $data['data_produk']=produkModel::get_data();
        return view('dashboard.dashboard',$data);
    }
}
