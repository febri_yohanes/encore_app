    @if (app()->isLocal())

        <!-- Required vendors -->
        <script src="{{ asset('/template/vendor/global/global.min.js') }}"></script>
        <script src="{{ asset('/template/js/quixnav-init.js') }}"></script>
        <script src="{{ asset('/template/js/custom.min.js') }}"></script>


        <!-- Vectormap -->
        <script src="{{ asset('/template/vendor/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('/template/vendor/morris/morris.min.js') }}"></script>


        <script src="{{ asset('/template/vendor/circle-progress/circle-progress.min.js') }}"></script>
        <script src="{{ asset('/template/vendor/chart.js/Chart.bundle.min.js') }}"></script>

        <script src="{{ asset('/template/vendor/gaugeJS/dist/gauge.min.js') }}"></script>

        <!--  flot-chart js -->
        <script src="{{ asset('/template/vendor/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('/template/vendor/flot/jquery.flot.resize.js') }}"></script>

        <!-- Owl Carousel -->
        <script src="{{ asset('/template/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>

        <!-- Counter Up -->
        <script src="{{ asset('/template/vendor/jqvmap/js/jquery.vmap.min.js') }}"></script>
        <script src="{{ asset('/template/vendor/jqvmap/js/jquery.vmap.usa.js') }}"></script>
        <script src="{{ asset('/template/vendor/jquery.counterup/jquery.counterup.min.js') }}"></script>

        <!-- Datatable -->
        <script src="{{ asset('/template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('/template/js/plugins-init/datatables.init.js') }}"></script>
        <script src="{{ asset('/template/js/dashboard/dashboard-1.js') }}"></script>
    @else
        <!-- Required vendors -->
        <script src="{{ asset('/public/template/vendor/global/global.min.js') }}"></script>
        <script src="{{ asset('/public/template/js/quixnav-init.js') }}"></script>
        <script src="{{ asset('/public/template/js/custom.min.js') }}"></script>


        <!-- Vectormap -->
        <script src="{{ asset('/public/template/vendor/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('/public/template/vendor/morris/morris.min.js') }}"></script>


        <script src="{{ asset('/public/template/vendor/circle-progress/circle-progress.min.js') }}"></script>
        <script src="{{ asset('/public/template/vendor/chart.js/Chart.bundle.min.js') }}"></script>

        <script src="{{ asset('/public/template/vendor/gaugeJS/dist/gauge.min.js') }}"></script>

        <!--  flot-chart js -->
        <script src="{{ asset('/public/template/vendor/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('/public/template/vendor/flot/jquery.flot.resize.js') }}"></script>

        <!-- Owl Carousel -->
        <script src="{{ asset('/public/template/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>

        <!-- Counter Up -->
        <script src="{{ asset('/public/template/vendor/jqvmap/js/jquery.vmap.min.js') }}"></script>
        <script src="{{ asset('/public/template/vendor/jqvmap/js/jquery.vmap.usa.js') }}"></script>
        <script src="{{ asset('/public/template/vendor/jquery.counterup/jquery.counterup.min.js') }}"></script>

        <!-- Datatable -->
        <script src="{{ asset('/public/template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('/public/template/js/plugins-init/datatables.init.js') }}"></script>
        <script src="{{ asset('/public/template/js/dashboard/dashboard-1.js') }}"></script>
    @endif
