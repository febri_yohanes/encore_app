@extends('layout.app')
@section('pagespecificcss')
@endsection
@php
use SimpleSoftwareIO\QrCode\Facades\QrCode;
@endphp
@if (app()->isLocal())
    <!-- Datatable -->
    <link href="{{ asset('template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <!-- Custom Stylesheet -->
    <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
@else
    <!-- Datatable -->
    <link href="{{ asset('public/template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <!-- Custom Stylesheet -->
    <link href="{{ asset('public/template/css/style.css') }}" rel="stylesheet">
@endif
@section('content')

    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Hi, Admin</h4>
                        <p class="mb-0">Generate Barcode</p>
                    </div>
                </div>
                <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Generate</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Barcode</a></li>
                    </ol>
                </div>
            </div>
            <div class="row">

                @if ($errors->any())
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </strong>
                        </div>
                    </div>

                @endif
                @if (Session::has('msg'))
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                Data Berhasil Dihapus
                            </strong>
                        </div>
                    </div>

                @endif
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Generate Barcode</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{ url('/produk') }}" class="badge badge-rounded badge-success"
                                        style="display: block;">Kembali</a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ url('/kode_produksi/generate/') . '/' . encrypt($id_produk) }}"
                                        class="badge badge-rounded badge-info" style="display: block;">Refresh</a>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ url('/kode_produksi/cetak_all/') . '/' . $id_produk }}" target="__newblank"
                                        class="badge badge-rounded badge-primary">Cetak Barcode
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            Toggle column: <a class="toggle-vis" data-column="1">Kode Produksi</a> - <a
                                class="toggle-vis" data-column="2">Link QR Code</a> - <a class="toggle-vis"
                                data-column="3">thumbnail</a> - <a class="toggle-vis" data-column="4">Nama Produk</a> -
                            <a class="toggle-vis" data-column="5">Size</a>
                            <div class="table-responsive">
                                <table id="datatable" class="display" style="min-width: 200px;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Produksi</th>
                                            <th>Kode Detail</th>
                                            <th>QR Code</th>
                                            <th>Status</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($data_produk as $item)
                                            <tr>
                                                <td>
                                                    {{ $no++ }} </td>
                                                <td>
                                                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($item->kode_produksi . 'xxxx', 'C39+', 1, 33, [28, 41, 81], true) }}"
                                                        alt="barcode" />
                                                </td>
                                                <td>
                                                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($item->detail_kode_produksi, 'C39+', 1, 33, [28, 41, 81], true) }}"
                                                        alt="barcode" />
                                                </td>
                                                <td>
                                                    {{ QrCode::generate($item->link) }}
                                                </td>
                                                <th>
                                                    @if ($item->status == 1)
                                                        <span class="badge badge-info">Sudah di cetak</span>
                                                    @else
                                                        <span class="badge badge-danger">Belum di cetak</span>
                                                    @endif
                                                </th>
                                                <td><a href="{{ url('/kode_produksi/print_barcode/') . '/' . encrypt($item->detail_kode_produksi) }}"
                                                        target="__blank" class="btn btn-success">Print</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Produksi</th>
                                            <th>Kode Detail</th>
                                            <th>QR Code</th>
                                            <th>Status</th>
                                            <th>#</th>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                        <div class="card-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificjs')

@endsection
@if (app()->isLocal())
    <script src="{{ asset('template/vendor/global/global.min.js') }}"></script>
    <script src="{{ asset('template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
@else
    <script src="{{ asset('public/template/vendor/global/global.min.js') }}"></script>
    <script src="{{ asset('public/template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
@endif
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "scrollY": "400px",

        });

        $('a.toggle-vis').on('click', function(e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });


    });
</script>
