<?php

namespace App\Models\transaksi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
class t_kode_produksi extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 't_kode_produksi';
    protected $primaryKey  = 'id_kode_produksi';   
    protected $guarded = [];

    public static function get_count_data_include_delete($id){
        return DB::table('t_kode_produksi')
        ->select(
            't_kode_produksi.kode_produksi'
        )
        ->where('t_kode_produksi.kode_produksi',$id)
        ->count();
    }

    public static function get_data($id){
        return DB::table('t_kode_produksi')
        ->select(
            't_kode_produksi.*',
            'm_produk.*',
            'm_jenis_ukuran.kode_ukuran'
        )
       
        ->rightJoin('m_produk','m_produk.kode_produksi','t_kode_produksi.kode_produksi')
        ->Join('m_jenis_ukuran','m_jenis_ukuran.id_ukuran','m_produk.id_ukuran')
        ->where('t_kode_produksi.kode_produksi',$id)
        ->orderBy('t_kode_produksi.detail_kode_produksi','ASC')
        ->get();
    }

    public static function get_first($id){
        return DB::table('t_kode_produksi')
        ->select(
            't_kode_produksi.*',
            'm_produk.*',
            'm_jenis_ukuran.kode_ukuran'
        )
        ->rightJoin('m_produk','m_produk.kode_produksi','t_kode_produksi.kode_produksi')
        ->Join('m_jenis_ukuran','m_jenis_ukuran.id_ukuran','m_produk.id_ukuran')
        ->where('t_kode_produksi.detail_kode_produksi',$id)
        ->first();
    }
    

}
