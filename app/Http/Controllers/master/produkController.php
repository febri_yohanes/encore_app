<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;
// Request Models
use App\Models\master\kategoriProdukModel;
use App\Models\master\ukuranModel;
use App\Models\master\produkModel;
use Image;
class produkController extends Controller
{
    public function index(){

        $data['ukuran_baju']=ukuranModel::all();
        $data['kategori']= kategoriProdukModel::all();
        $data['data_produk']=produkModel::get_data();
       
        return view('master.produk.produk',$data);
    }

    public function proses_input_produk(Request $request){
        // Generate UUID
   
        $this->validate($request, [
			'nama_produksi' => 'required',
			'stock' => 'required',
			'size' => 'required',	
            'harga' => 'required',
            'gender' => 'required',
            'nama_jenis' => 'required',
			'qrcode' => 'required',
            'thumbnail' => 'required|mimes:jpeg,png,jpg,pdf|max:10000',
		]);

        $rand = rand(1111, 9999);
        $kode = NULL;
        $thumbnail = $request->file('thumbnail');
        $input['imagename'] = "file_thumbnail" . date("Ymd").$rand . '-' . date("Y-m-d").'.'.$thumbnail->getClientOriginalExtension();
        // $thumbnail->move('upload/thumbnail', "file_thumbnail" . date("Ymd").$rand . '-' . date("Y-m-d") . '.' .$thumbnail->getClientOriginalExtension());
        // =====================
        $destinationPath = public_path('upload/thumbnail');
        $img = Image::make($thumbnail->getRealPath());
        $img->resize(320, 320, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);
        // dd($img);
        // $destinationPath = public_path('upload/thumbnail');
        // $img->move($destinationPath, $input['imagename']);
 
        // =====================
        $uuid = Str::uuid();
        do {

            $uuid = Str::uuid();
        } while (produkModel::find($uuid));
        
        do {
            $kode = date("Y").date("m").rand(1111, 9999);
        } while (produkModel::where('kode_produksi','=', $kode)->first());
     
        produkModel::create([
            'id_produk' => $uuid,
            'kode_produksi' => $kode,
            'gender' => $request->gender,
            'nama_produk' => $request->nama_produksi,
            'id_jenis' => $request->nama_jenis,
			'stock' => $request->stock,
            'harga' => $request->harga,
			'id_ukuran' => $request->size,
			'link' => $request->qrcode,
            'thumbnail' => $input['imagename'],
        ]);
        return redirect('/produk');
    }

    public function form_edit_produk($id){
    
        $data['ukuran_baju']=ukuranModel::all();
        $data['kategori']= kategoriProdukModel::all();
        $data['data_produk']=produkModel::get_data_by_id(decrypt($id));
    

        return view('master.produk.edit_produk',$data);
    
    }

    public function proses_edit_produk(Request $request){
        $this->validate($request, [
            'id_produk' => 'required',
			'nama_produksi' => 'required',
			'stock' => 'required',
			'size' => 'required',	
            'harga' => 'required',
            'nama_jenis' => 'required',
			'qrcode' => 'required',
            
		]);

        if($request->thumbnail){
            $this->validate($request, [
				'thumbnail' => 'required|mimes:jpeg,png,jpg,pdf|max:10000',
			]);
            $rand = rand(1111, 9999);
            $thumbnail = $request->file('thumbnail');
            $thumbnail->move('upload/thumbnail', "file_thumbnail" . date("Ymd").$rand . '-' . date("Y-m-d") . '.' .$thumbnail->getClientOriginalExtension());
        }
        
     
        $data = produkModel::find($request->id_produk);
		$data->nama_produk = $request->nama_produksi;
		$data->stock = $request->stock;
		$data->id_ukuran = $request->size;
        $data->harga = $request->harga;
        $data->link = $request->qrcode;
        $data->id_jenis = $request->nama_jenis;
        if($request->thumbnail){
        $data->thumbnail = "file_thumbnail" . date("Ymd").$rand . '-' . date("Y-m-d") . '.' .$thumbnail->getClientOriginalExtension();
        }
        $data->save();
        
        return redirect("/produk/edit_produk/".encrypt($request->id_produk))->with('msg', 'Data berhasil Dirubah');
    }

    public function proses_delete_produk($id){
    
        $data = produkModel::find(decrypt($id));
		$data->delete();
        return redirect('/produk')->with('msg', 'Data berhasil Dihapus');
    
    }



}
