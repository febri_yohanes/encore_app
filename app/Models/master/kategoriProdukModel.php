<?php

namespace App\Models\master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
class kategoriProdukModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'm_jenis_produk';
    protected $primaryKey  = 'id_jenis';   
    protected $guarded = [];
}
