@extends('layout.app')
@section('pagespecificcss')
@endsection
@php
use SimpleSoftwareIO\QrCode\Facades\QrCode;
@endphp

@if (app()->isLocal())
<!-- Datatable -->
<link href="{{ asset('template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<!-- Custom Stylesheet -->
<link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
@else
<!-- Datatable -->
<link href="{{ asset('public/template/vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<!-- Custom Stylesheet -->
<link href="{{ asset('public/template/css/style.css') }}" rel="stylesheet">
@endif
@section('content')

    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                <div class="col-sm-6 p-md-0">
                    <div class="welcome-text">
                        <h4>Hi, Admin</h4>
                        <p class="mb-0">Management Produk</p>
                    </div>
                </div>
                <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Produk</a></li>
                    </ol>
                </div>
            </div>
            <div class="row">

                @if ($errors->any())
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </strong>
                        </div>
                    </div>

                @endif
                @if (Session::has('msg'))
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissible alert-alt fade show">
                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i
                                        class="mdi mdi-close"></i></span>
                            </button>
                            <strong>
                                Data Berhasil Dihapus
                            </strong>
                        </div>
                    </div>

                @endif
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Management Produk</h4>
                            <button type="button" class="badge badge-rounded badge-primary" data-toggle="modal"
                                data-target=".bd-example-modal-lg">Tambah Produk</button>
                        </div>
                        <div class="card-body">
                            Toggle column: <a class="toggle-vis" data-column="1">Kode Produksi</a> - <a
                                class="toggle-vis" data-column="2">Link QR Code</a> - <a class="toggle-vis"
                                data-column="3">thumbnail</a> - <a class="toggle-vis" data-column="4">Nama Produk</a> -
                            <a class="toggle-vis" data-column="5">Size</a> - <a class="toggle-vis"
                                data-column="6">Jumlah Stock</a> - <a class="toggle-vis" data-column="7">Harga</a>
                            <div class="table-responsive">
                                <table id="datatable" class="display" style="min-width: 200px;display:block">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Produksi</th>
                                            <th>Link QR Code</th>
                                            <th style="max-width: 10%">thumbnail</th>
                                            <th>Nama Produk</th>
                                            <th>Size</th>
                                            <th>gender</th>
                                            <th>Jumlah Stock</th>
                                            <th>Harga</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($data_produk as $item)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>
                                                    <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($item->kode_produksi . 'xxxx', 'C39+', 1, 33, [28, 41, 81], true) }}"
                                                        alt="barcode" />
                                                </td>
                                                <td>{{ QrCode::generate($item->link) }}</td>
                                                <th>
                                                    @if (app()->isLocal())
                                                    <img src="{{ asset('upload/thumbnail/') . '/' . $item->thumbnail }}"
                                                        style="display:block;" width="100%" height="100%" alt="image"
                                                        class="">
                                                    @else
                                                    <img src="{{ asset('upload/thumbnail/') . '/' . $item->thumbnail }}"
                                                        style="display:block;" width="100%" height="100%" alt="image"
                                                        class="">
                                                    
                                                    @endif
                                                </th>
                                                <th>{{ $item->nama_produk }}</th>
                                                <th>{{ $item->kode_ukuran }}</th>
                                                <th>{{ $item->gender }}</th>
                                                <th>{{ $item->stock }}</th>
                                                <td>Rp.{{ number_format($item->harga, 2, ',', '.') }}</td>
                                                <td><a href="
                                                        {{ url('/') }}/kode_produksi/generate/{{ encrypt($item->kode_produksi) }}"
                                                        class="badge badge-rounded badge-success">Print</a>
                                                    <a href="
                                                            {{ url('/') }}/produk/edit_produk/{{ encrypt($item->id_produk) }}"
                                                        class="badge badge-rounded badge-primary">Edit</a>
                                                    <a href="{{ url('/') }}/produk/delete_produk/{{ encrypt($item->id_produk) }}"
                                                        class="badge badge-rounded badge-danger">Delete</a>
                                            </tr>
                                        @endforeach


                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Produksi</th>
                                            <th>Link QR Code</th>
                                            <th style="max-width: 10%">thumbnail</th>
                                            <th>Nama Produk</th>
                                            <th>Size</th>
                                            <th>gender</th>
                                            <th>Jumlah Stock</th>
                                            <th>Harga</th>
                                            <th>#</th>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                        <div class="card-footer">
                            @php
                                $arr = [];
                                $index = 0;
                                foreach ($data_produk as $i) {
                                    $arr[$index]['kode_produksi'] = $i->kode_produksi;
                                    $arr[$index]['nama_produk'] = $i->nama_produk;
                                    $arr[$index]['total_asset'] = $i->stock * $i->harga;
                                    $index++;
                                }
                                $jumlah = 0;
                                $temp = 0;
                                foreach ($arr as $val) {
                                    $jumlah = $val['total_asset'] + $temp;
                                    $temp = $jumlah;
                                }
                                
                            @endphp

                            Total Aset :<b>Rp.{{ number_format($jumlah, 2, ',', '.') }} </b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal untuk tambah Produk --}}
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="POST" action="{{ url('/produk/proses_input_data') }}" id="form1"
                    enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Masukan Nama produk</label>
                            <input type="text" name="nama_produksi" class="form-control input-default "
                                placeholder="Nama Produk" required>
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="number" name="harga" class="form-control input-default " placeholder="Rp." required>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Stock Barang</label>
                            <input type="number" name="stock" class="form-control input-default "
                                placeholder="Jumlah Stock Barang" required>
                        </div>
                        <div class="form-group">
                            <label>Ukuran / Size Baju</label>
                            <select class="form-control" name="size" id="sel1">
                                @foreach ($ukuran_baju as $item)
                                    <option value="{{ $item->id_ukuran }}">{{ $item->kode_ukuran }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis Produk</label>
                            <select class="form-control" name="nama_jenis" id="sel1">
                                @foreach ($kategori as $item)
                                    <option value="{{ $item->id_jenis }}">{{ $item->nama_jenis }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control" name="gender" id="sel1">

                                <option value="m">Male</option>
                                <option value="f">Female</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Link QR Code</label>
                            <input type="text" name="qrcode" class="form-control input-default " placeholder="https:://" required>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Upload thumbnail (Optional)</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" name="thumbnail" class="custom-file-input" required>
                                    <label class="custom-file-label">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal --}}
@endsection

@section('pagespecificjs')

@endsection
@if (app()->isLocal())
<script src="{{ asset('template/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
@else
<script src="{{ asset('public/template/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('public/template/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>

@endif
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "scrollY": "400px",

        });

        $('a.toggle-vis').on('click', function(e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });


    });
</script>
