<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;
use App\Http\Controllers\master\produkController;
use App\Http\Controllers\transaksi\kode_produksiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (App::environment('production')) {
  URL::forceScheme('https');
  Config::set('app.debug', false);
} else {
  // Route::get('/tester/show_session', function() { echo "<pre>".print_r(Session::all(), true)."</pre>"; });
}

Route::get('/apps', [AppController::class, 'index']);
Route::get('/produk', [produkController::class, 'index']);
Route::post('/produk/proses_input_data', [produkController::class, 'proses_input_produk']);
Route::get('/produk/delete_produk/{id}', [produkController::class, 'proses_delete_produk']);
Route::get('/produk/edit_produk/{id}', [produkController::class, 'form_edit_produk']);
Route::post('/produk/proses_update_data', [produkController::class, 'proses_edit_produk']);
//Generate kode_produksi
Route::get('/kode_produksi/generate/{id}', [kode_produksiController::class, 'index']);
Route::get('/kode_produksi/cetak_all/{id}', [kode_produksiController::class, 'cetak_all']);
Route::get('/kode_produksi/print_barcode/{id}', [kode_produksiController::class, 'generate_barcode']);

