@php
use SimpleSoftwareIO\QrCode\Facades\QrCode;

@endphp
 


<body>

    <style type="text/css">
        table {
            border-collapse: collapse;

            max-height: 100%;
        }

        table tr td,
        table tr th {
            font-size: 9pt;
            border-collapse: collapse;

        }

        h1 {
            font-family: Arial Narrow, Arial, sans-serif;
            font: bold 30px/1;
        }

        p {
            font-family: Georgia, 'Times New Roman', Times, serif;

        }

        .judul {
            font-family: Helvetica Neue, Helvetica;
            font: 80px/1;
        }

        .logo {
            margin-left: -13px;
            width: 355px;
            height: 190px;
            margin-top: -35px;
            margin-bottom: -25px;
        }

        .qrcode {
            width: 210px;
            height: 210px;
            margin-top: -35px;
        }

        #harga {
            margin-top: -30;
            font-size: 60px;
        }

        #tulisan_tengah {}

        #cotton_id {
            max-height: 10px;
        }

        .barcode {
            display: flex;
            align: center;
            width: 82%;
            height: 90px;
            color: black;
        }

        .size_baju {

            letter-spacing: -5px;
            font: bold 92px/1 Helvetica;
        }

        .made {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            font: bold 20px/1 sans-serif;
            margin-top: -30px;

        }

        div.page_break+div.page_break {
            page-break-before: always;
        }

        table,
        th,
        td {
            /* border: 1px solid black;
            border-collapse: collapse; */
        }

        hr {
            border-top: 1px dashed;
        }

        p {
            font-size: 30px;
        }

    </style>



    <table style="width:100%">
        <tr>
            <td colspan="2" style="max-width:80%">
                <label class="judul">{{ strtoupper($t_kode_produksi->nama_produk) }} (<?php if ($t_kode_produksi->gender == 'm') {
    echo 'MEN';
} else {
    echo 'WOMEN';
} ?>)</label>
            </td>
        </tr>
        <tr>
            <td>
                <img class="logo" src="{{ public_path('img/encore2.png') }}" alt="">
            </td>
            <td style="text-align:center;">
                <label class="made">
                    Made In Indonesia
                </label>
            </td>
        </tr>
        <tr>
            <td valign="top" id="cotton_id" rowspan="1">
                <label id="harga">IDR{{ number_format($data->harga, 2, ',', '.') }}</label>
                <p style="text-align:left; font-size: 38px; margin-top:-5px;">70% BAMBOO 30% COTTON</p>
                <p style="text-align:center; font-size: 35px; margin-top:-30px; margin-bottom:15;"><i>"Please pay
                        attention to wash care instructions<br> for durability"</i></p>
            </td>
            <td style="text-align: center;" rowspan="1">
                <img class="qrcode"
                    src="data:image/png;base64, {{ base64_encode(QrCode::generate($t_kode_produksi->link)) }} ">

            </td>
        </tr>
        <tr>
            <td style="text-align: left; max-width:60%;">
                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($t_kode_produksi->detail_kode_produksi, 'C39', 1, 33, [0, 0, 0], true) }}"
                    alt="barcode" class="barcode" />
            </td>
            <td style="text-align: center;" valign="bottom" colspan="1">
                <label class="size_baju">
              
                    {{ strtoupper($t_kode_produksi->kode_ukuran) }}
                </label>
            </td>

        </tr>
    </table>
    <br>
    <div class="page_break"></div>





</body>

</html>
<style>
</style>
